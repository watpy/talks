'''Example of inheritence and super()'''

class A(object):
  def __init__(self):
    print ('Init A')

  def a_func_only(self):
    print ('Calling A function only')

class B(object):
  def __init__(self):
    print ('Init B')

class C(A, B):
  def __init__(self):
    print ('Init C')
    super().__init__()
    super().a_func_only()

class D(B, A):
  def __init__(self):
    print ('Init D')
    super().__init__()
    super().a_func_only()

class E(D):
  def __init__(self):
    print ('Init E')
    super().__init__()

class F(C):
  def __init__(self):
    print ('Init F')
    super().__init__()
