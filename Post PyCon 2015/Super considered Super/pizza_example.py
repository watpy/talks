'''Raymond Hettinger example of inheritence and super()'''

class CrapDoughFactory(object):
    def get_dough(self):
      return 'bad pizza dough'

class Pizza(CrapDoughFactory):
    def order_pizza(self, *toppings):
        print ('getting dough')
        dough = super().get_dough()
        print ('making pie with %s' % dough)
        for topping in toppings:
            print ('Adding: %s' % topping)

class OrganicDoughFactory(CrapDoughFactory):
    def get_dough(self):
        return 'good pizza dough'

class OrganicPizza(Pizza, OrganicDoughFactory):
    pass
