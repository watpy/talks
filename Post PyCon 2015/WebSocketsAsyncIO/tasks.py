import os
from os import path

from invoke import task, run


BUILD_DIR = 'www'
EXTRA_DIRS = ['__pycache__', BUILD_DIR + '/.doctrees', BUILD_DIR + '/_sources']


@task
def slides():
    if not path.exists(BUILD_DIR):
        os.mkdir(BUILD_DIR)
    run('sphinx-build -b slides . {}'.format(BUILD_DIR))


@task
def clean():
    run('rm {} -rvf'.format(BUILD_DIR))


@task(clean, slides)
def publish():
    for directory in EXTRA_DIRS:
        run('rm {} -rvf'.format(directory))
